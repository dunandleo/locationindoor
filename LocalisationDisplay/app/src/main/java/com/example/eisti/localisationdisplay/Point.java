package com.example.eisti.localisationdisplay;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;


/**
 * Created by eisti on 19/05/17.
 */

public class Point {
    private int x;
    private int y;
    private float dir;


    public Point() {
        x=0;
        y=0;
    }

    public void setDir(int dir)
    {
        this.dir=dir;
    }

    public void draw(Canvas canvas)
    {
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        canvas.drawCircle(x , y , 15, paint);
    }

    public void resize(int wScreen, int hScreen) {

    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public float getDir() {
        return dir;
    }

    public void setDir(float dir) {
        this.dir = dir;
    }

    public float convertXPos(int width, int maxGrid)
    {
        float newX = (this.x * maxGrid);
        newX = newX/width;
        return newX;
    }

    public float convertYPos(int height, int maxGrid)
    {
        float newY = (this.y * maxGrid);
        newY = newY/height;
        return newY;
    }
}