import csv
import numpy as np
from numpy import array
import matplotlib.pyplot as plt
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
from sklearn2pmml import sklearn2pmml
from sklearn_pandas import DataFrameMapper
from sklearn2pmml import PMMLPipeline
from sklearn.externals import joblib
import pickle





def notInList(list,toFind):
	boo = True
	x=0
	while (x<len(list)):
		if (toFind==list[x]):
			boo=False
		x=x+1
	return boo

def searchIndex(list,toFind):
	count=0
	x=0
	while (x<len(list)):
		if (toFind==list[x]):
			count=x
		x=x+1
	return count

def initiList(listaRemplir,l):
	x=0
	listaRemplir=[]
	while (x<l):
		listaRemplir.append(0)
		x=x+1
	listaRemplir.append(-1)
	return listaRemplir



listSSID=[]
with open("test.txt") as file:
    for line in file: 
        line = line.strip() #or some other preprocessing
        listSSID.append(line) #storing everything in memory!

listVar=[]
listVar.append("Direction")
with open('newData.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    for row in readCSV:
        if (row[2].replace(" ", "")!=""):
        	if (notInList(listSSID,row[2])):
        		listSSID.append(row[2])
        		listVar.append(row[2])

listX=[]
listY=[]
listRssDir=[]
listRssDir=initiList(listRssDir,len(listSSID))
with open('newData.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    for row in readCSV:
        if (row[0]!=""):
        	index=searchIndex(listSSID,row[2])
        	listRssDir[index]=float(row[3])
        	if (listRssDir[len(listRssDir)-1]==-1):
        		listRssDir[len(listRssDir)-1]=float(row[4])
        		listY.append((float(row[0]),float(row[1])))
        else:
        	if (len(listRssDir)>0):
        		listX.append(listRssDir)
        		listRssDir=initiList(listRssDir,len(listSSID))
listX.append(listRssDir)

testX=[]
testY=[]
listRssDir=[]
listRssDir=initiList(listRssDir,len(listSSID))
with open('testdata.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    for row in readCSV:
        if (row[0]!=""):
        	if (not(notInList(listSSID,row[2]))): 
	        	index=searchIndex(listSSID,row[2])
	        	listRssDir[index]=float(row[3])
	        	if (listRssDir[len(listRssDir)-1]==-1):
	        		listRssDir[len(listRssDir)-1]=float(row[4])
	        		testY.append((float(row[0]),float(row[1])))
        else:
        	if (len(listRssDir)>0):
        		testX.append(listRssDir)
        		listRssDir=initiList(listRssDir,len(listSSID))
testX.append(listRssDir)

print(testX[4])
print(testY[4])



#X is the independent variable 
X = listX
#vector is the dependent data
vectorX = []
vectorY = []

count = 0
while (count<len(listY)):
	vectorX.append(listY[count][0])
	vectorY.append(listY[count][1])
	count=count+1

#predict is an independent variable for which we'd like to predict the value
predict=[[99.0, 94.0, 90.0, 83.0, 70.0, 30.0, 50.0, 48.0, 41.0, 52.0, 0, 24.0, 41.0, 44.0, 33.0, 19.0, 0, 17.0, 0, 0, 35.0, 0, 0, 17.0, 0, 0, 0, 0, 17.0, 0, 15.0, 0, 15.0, 0, 15.0, 13.0, 0, 0, 0, 0, 0, 0, 0, 17.0, 0, 0, 15.0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 61.0]]
#generate a model of polynomial features
poly = PolynomialFeatures(degree=2)

#transform the x data for proper fitting (for single variable type it returns,[1,x,x**2])
X_ = poly.fit_transform(X)

#transform the prediction to fit the model type
predict_ = poly.fit_transform(predict)

#here we can remove polynomial orders we don't want
#for instance I'm removing the `x` component
X_ = np.delete(X_,(1),axis=1)
predict_ = np.delete(predict_,(1),axis=1)

#generate the regression object
clfX = LinearRegression()
clfY = LinearRegression()


#preform the actual regression
clfX.fit(X_, vectorX)
clfY.fit(X_, vectorY)




pipeX = PMMLPipeline([("regression", LinearRegression())])
pipeX.fit(X_, vectorX)
pipeY = PMMLPipeline([("regression", LinearRegression())])
pipeY.fit(X_, vectorY)

sklearn2pmml(pipeX,"px.pmml")
sklearn2pmml(pipeY,"py.pmml")

with open("test.txt","w") as file:
	for item in listSSID:
  		file.write("%s\n" % item)
