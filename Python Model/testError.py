import csv
import numpy as np
from numpy import array
import matplotlib.pyplot as plt
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
from mpl_toolkits.mplot3d import Axes3D
import pickle
from sklearn_porter import Porter

deg=1

def convertInMeter(x):
	return x*0.5

def notInList(list,toFind):
	boo = True
	x=0
	while (x<len(list)):
		if (toFind==list[x]):
			boo=False
		x=x+1
	return boo

def searchIndex(list,toFind):
	count=0
	x=0
	while (x<len(list)):
		if (toFind==list[x]):
			count=x
		x=x+1
	return count

def initiList(listaRemplir,l):
	x=0
	listaRemplir=[]
	while (x<l):
		listaRemplir.append(0)
		x=x+1
	listaRemplir.append(-1)
	return listaRemplir

# function predict
def prediction(toPred, clf):
	#generate a model of polynomial features
	poly = PolynomialFeatures(degree=deg)
	#transform the prediction to fit the model type
	predict_ = poly.fit_transform(toPred)
	predict_ = np.delete(predict_,(1),axis=1)
	return clf.predict(predict_)



listSSID=[]
with open('newData.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    for row in readCSV:
        if (row[0]!=""):
        	if (notInList(listSSID,row[2])):
        		listSSID.append(row[2])

listX=[]
listY=[]
listRssDir=[]
listRssDir=initiList(listRssDir,len(listSSID))
with open('newData.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    for row in readCSV:
        if (row[0]!=""):
        	index=searchIndex(listSSID,row[2])
        	listRssDir[index]=float(row[3])
        	if (listRssDir[len(listRssDir)-1]==-1):
        		listRssDir[len(listRssDir)-1]=float(row[4])
        		listY.append((float(row[0])*0.5,float(row[1])*0.5))
        else:
        	if (len(listRssDir)>0):
        		listX.append(listRssDir)
        		listRssDir=initiList(listRssDir,len(listSSID))
listX.append(listRssDir)

testX=[]
testY=[]
listRssDir=[]
listRssDir=initiList(listRssDir,len(listSSID))
with open('testdata.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    for row in readCSV:
        if (row[0]!=""):
        	if (not(notInList(listSSID,row[2]))): 
	        	index=searchIndex(listSSID,row[2])
	        	listRssDir[index]=float(row[3])
	        	if (listRssDir[len(listRssDir)-1]==-1):
	        		listRssDir[len(listRssDir)-1]=float(row[4])
	        		testY.append((float(row[0])*0.5,float(row[1])*0.5))
        else:
        	if (len(listRssDir)>0):
        		testX.append(listRssDir)
        		listRssDir=initiList(listRssDir,len(listSSID))
testX.append(listRssDir)



#X is the independent variable 
X = listX
#vector is the dependent data
vector = listY

#predict is an independent variable for which we'd like to predict the value
predict=[[99.0, 94.0, 90.0, 83.0, 70.0, 30.0, 50.0, 48.0, 41.0, 52.0, 0, 24.0, 41.0, 44.0, 33.0, 19.0, 0, 17.0, 0, 0, 35.0, 0, 0, 17.0, 0, 0, 0, 0, 17.0, 0, 15.0, 0, 15.0, 0, 15.0, 13.0, 0, 0, 0, 0, 0, 0, 0, 17.0, 0, 0, 15.0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 61.0]]
#generate a model of polynomial features
poly = PolynomialFeatures(degree=deg)

#transform the x data for proper fitting (for single variable type it returns,[1,x,x**2])
X_ = poly.fit_transform(X)

#transform the prediction to fit the model type
predict_ = poly.fit_transform(predict)

#here we can remove polynomial orders we don't want
#for instance I'm removing the `x` component
X_ = np.delete(X_,(1),axis=1)
predict_ = np.delete(predict_,(1),axis=1)

#generate the regression object
clf = LinearRegression()


#preform the actual regression
clf.fit(X_, vector)


errorX = 0
errorY = 0
maxError=0
minError=-1
listError=[]
error=0
totalError=0
ly=[]
lx=[]
count=0
while (count<len(testX)):
	errorX = prediction([testX[count]],clf)[0][0] - testY[count][0]
	errorY = prediction([testX[count]],clf)[0][1] - testY[count][1]
	error= (errorX**2 + errorY**2)**0.5
	if (error<minError):
		minError=error

	if (minError<0):
		minError=error
	else:	 
		if (error>maxError):
			maxError=error

	totalError= totalError + error
	listError.append(error)
	lx.append(testY[count][0])
	ly.append(testY[count][1])

	count=count+1

print((totalError)/count)
print(minError)
print(maxError)



fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')



ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Error')
ax.set_xlim(0,7)
ax.set_ylim(0,7)
ax.set_zlim(0,10)

count=0
while (count<len(lx)):
	ax.plot([lx[count], lx[count]], [ly[count], ly[count]], [0,listError[count]],c='r',linewidth=2)
	
	count=count+1


plt.show()



