package com.example.eisti.locatingoffline;

/**
 * Created by eisti on 09/05/18.
 */

public class WifiClass {
    public String SSID;
    public int RSS;

    public int getRSS() {
        return RSS;
    }

    public String getSSID() {
        return SSID;
    }



    public WifiClass(String pSSID, int pRSS)
    {
        this.SSID = pSSID;
        this.RSS = pRSS;

    }
}
