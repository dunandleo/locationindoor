package com.example.eisti.locatingoffline;

import android.Manifest;
import android.app.ActionBar;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends AppCompatActivity implements SensorEventListener {


    private TextView finalSSid;
    private TextView finalRss;
    private NumberPicker xEt;
    private NumberPicker yEt;
    private EditText dirEt;
    private SensorManager mSensorManager;
    private boolean dirChanged = false;
    private boolean fileCreate = false;
    private String fileName;


    //Broadcast receiver to save the list of wifi in a CSV
    private BroadcastReceiver receiverSave = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            xEt = (NumberPicker) findViewById(R.id.xEditText);
            yEt = (NumberPicker) findViewById(R.id.yEditText);
            final WifiManager wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            int state = wifi.getWifiState();
            if (state == WifiManager.WIFI_STATE_ENABLED) {
                List<ScanResult> results = wifi.getScanResults();
                String x = Float.toString(xEt.getValue());
                String y = Float.toString(yEt.getValue());
                String dir = dirEt.getText().toString();
                if (x.equals("") || y.equals("") || dir.equals(""))
                {
                    Toast.makeText(MainActivity.this,
                            "Enter correct position and direction", Toast.LENGTH_LONG).show();
                }
                else {
                    results = sortTab(results);
                    String content = "";
                    Date dt = new Date();
                    int hours = dt.getHours();
                    int minutes = dt.getMinutes();
                    int seconds = dt.getSeconds();
                    Date todayDate = Calendar.getInstance().getTime();
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    String curDate = formatter.format(todayDate);
                    String curTime = hours + ":" + minutes + ":" + seconds;
                    for (ScanResult scanResult : results) {
                        int level = WifiManager.calculateSignalLevel(scanResult.level, 100);

                        content = content + x + "," + y + "," + String.valueOf(scanResult.SSID) + "," + String.valueOf(level) + "," + dir + "," + curDate + "," + curTime + "\n";
                    }
                    writeData(content, Environment.getExternalStorageDirectory() + "/Download/"+fileName+".csv");
                    Toast.makeText(MainActivity.this,
                            "Saved", Toast.LENGTH_LONG).show();
                }

            }
            else
            {
                Toast.makeText(MainActivity.this,
                        "Turn the wifi ON", Toast.LENGTH_LONG).show();
            }
            unregisterReceiver(this);
        }
    };


    public List<ScanResult> sortTab(List<ScanResult> results)
    {
        // Sorting
        Collections.sort(results, new Comparator<ScanResult>() {
            @Override
            public int compare(ScanResult scan1, ScanResult scan2)
            {
                Integer x1 = WifiManager.calculateSignalLevel(scan1.level, 10);
                Integer x2 = WifiManager.calculateSignalLevel(scan2.level, 10);
                return  x2.compareTo(x1);
            }
        });
        return results;
    }






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ActivityCompat.requestPermissions(MainActivity.this,
                new String[] {
                        Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE },
                2);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_action_bar_layout);

        dirEt = (EditText) findViewById(R.id.dirEditText);
        dirEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                {
                    dirChanged = true;
                }
                else
                {
                    if (dirEt.getText().toString().equals(""))
                    {
                        dirChanged = false;
                    }
                }
            }
        });


         // initialize your android device sensor capabilities
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        finalSSid = (TextView) findViewById(R.id.ssidText);
        finalRss = (TextView) findViewById(R.id.rssText);
        finalRss.setTypeface(finalRss.getTypeface(), Typeface.BOLD);
        new Timer().schedule(timerTask, 100,2);

        NumberPicker npX = (NumberPicker) findViewById(R.id.xEditText);
        npX.setMinValue(0);
        npX.setMaxValue(100);
        npX.setOnValueChangedListener(onValueChangeListener);

        NumberPicker npY = (NumberPicker) findViewById(R.id.yEditText);
        npY.setMinValue(0);
        npY.setMaxValue(100);
        npY.setOnValueChangedListener(onValueChangeListener);

    }

    NumberPicker.OnValueChangeListener onValueChangeListener =
            new 	NumberPicker.OnValueChangeListener(){
                @Override
                public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                    Toast.makeText(MainActivity.this,
                            "selected number "+numberPicker.getValue(), Toast.LENGTH_SHORT);
                }
            };


    @Override
    protected void onResume() {
        super.onResume();

        // for the system's orientation sensor registered listeners
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                SensorManager.SENSOR_DELAY_GAME);
    }


    @Override
    protected void onPause() {
        super.onPause();

        // to stop the listener and save battery
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        // get the angle around the z-axis rotated
        float degree = Math.round(event.values[0]);
        if (!dirChanged)
        {
            dirEt.setText(Float.toString(degree));
        }


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // not in use
    }




    TimerTask timerTask = new TimerTask() {
        @Override
        public void run() {
            final WifiManager wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            int state = wifi.getWifiState();
            String resSSid="";
            String resRss = "";
            if (state == WifiManager.WIFI_STATE_ENABLED) {
                wifi.startScan();
                List<ScanResult> results = wifi.getScanResults();

                //Prepare barchart
                ArrayList<BarEntry> entries = new ArrayList<>();
                ArrayList<String> labels = new ArrayList<String>();

                int count = 0;
                results = sortTab(results);
                for (ScanResult scanResult : results) {
                    int level = WifiManager.calculateSignalLevel(scanResult.level, 100);

                    resSSid = resSSid + String.valueOf(scanResult.SSID) + " " + "\n";
                    resRss = resRss + String.valueOf(level) + "\n";

                    //Add data to barchart
                    entries.add(new BarEntry(count,level));
                    labels.add("["+String.valueOf(level)+"]" + "   " +String.valueOf(scanResult.SSID));

                    count = count+1;

                }
                final String finalResSSid = resSSid;
                final String finalResRss = resRss;
                final ArrayList<BarEntry> finalEntries = entries;
                final ArrayList<String> finalLabels = labels;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        finalSSid.setText(finalResSSid);
                        finalRss.setText(finalResRss);

                        HorizontalBarChart chart = (HorizontalBarChart) findViewById(R.id.chart);
                        BarDataSet barDataSet = new BarDataSet(finalEntries,"");
                        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
                        dataSets.add(barDataSet );
                        BarData data = new BarData(dataSets);
                        chart.setData(data);
                        chart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(finalLabels));
                        chart.getXAxis().setLabelCount(finalEntries.size());
                        chart.notifyDataSetChanged();
                        chart.setDescription(null);
                        chart.invalidate();

                    }
                });
            }
            else
            {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        finalRss.setText("");
                        finalSSid.setText("");
                    }
                });
            }
        }
    };





    //Dynamic thread
    Thread dynamicScanThread = new Thread() {
        @Override
        public void run() {
           startHandler();
        }
    };


    //Void to write in csv file
    public void writeData(String data, String strFilePath) {

        PrintWriter csvWriter;
        try {

        /*1. declare stringBuffer*/
            StringBuffer oneLineStringBuffer = new StringBuffer();

            File file = new File(strFilePath);
            if (!file.exists()) {
                file.createNewFile();
                data = "X,Y,SSID,RSS,Direction,Date,Time\n"+data;

            }
            csvWriter = new PrintWriter(new FileWriter(file, true));

        /*2. append to stringBuffer*/
            oneLineStringBuffer.append(data);
            oneLineStringBuffer.append("\n");

        /*3. print to csvWriter*/
            csvWriter.print(oneLineStringBuffer);

            csvWriter.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //Void to write in csv file
    public void createFile() {

        PrintWriter csvWriter;
        try {

        /*1. declare stringBuffer*/
            StringBuffer oneLineStringBuffer = new StringBuffer();
            String data = "";
            File file = new File(Environment.getExternalStorageDirectory() + "/Download/"+fileName+".csv");
            if (!file.exists()) {
                try {
                    file.createNewFile();
                    Button recordButt = (Button) findViewById(R.id.recordButton);
                    recordButt.setVisibility(View.VISIBLE);
                    Button newfileButt = (Button) findViewById(R.id.newfileButton);
                    newfileButt.setVisibility(View.GONE);
                    csvWriter = new PrintWriter(new FileWriter(file, true));

        /*2. append to stringBuffer*/
                    oneLineStringBuffer.append(data);
                    oneLineStringBuffer.append("\n");

        /*3. print to csvWriter*/
                    csvWriter.print(oneLineStringBuffer);

                    csvWriter.close();
                    Toast.makeText(getApplication(),
                            "File created", Toast.LENGTH_SHORT).show();

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getBaseContext(), e.getMessage(),Toast.LENGTH_SHORT).show();

                }

            }
            else
            {
                Toast.makeText(getApplication(),
                        "File already exist", Toast.LENGTH_SHORT).show();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //Scan wifi and save list in csv when user click on the button
    public void onClickRecord(View v) throws IOException {


        registerReceiver(receiverSave, new IntentFilter(WifiManager.RSSI_CHANGED_ACTION));


    }

    //Create new file when user click
    public void onClickNew(View v) throws IOException {


        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertdialog_custom_view,null);

        // Specify alert dialog is not cancelable/not ignorable
        builder.setCancelable(false);

        // Set the custom layout as alert dialog view
        builder.setView(dialogView);

        // Get the custom alert dialog view widgets reference
        Button btn_positive = (Button) dialogView.findViewById(R.id.dialog_positive_btn);
        Button btn_negative = (Button) dialogView.findViewById(R.id.dialog_negative_btn);
        final EditText et_name = (EditText) dialogView.findViewById(R.id.et_name);

        // Create the alert dialog
        final AlertDialog dialog = builder.create();

        // Set positive/yes button click listener
        btn_positive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Dismiss the alert dialog
                dialog.cancel();
                fileName = et_name.getText().toString();
                createFile();
            }
        });

        // Set negative/no button click listener
        btn_negative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Dismiss/cancel the alert dialog
                //dialog.cancel();
                dialog.dismiss();
            }
        });

        // Display the custom alert dialog on interface
        dialog.show();


    }

    public void quitButton(View v)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.end_dialog,null);

        // Specify alert dialog is not cancelable/not ignorable
        builder.setCancelable(false);

        // Set the custom layout as alert dialog view
        builder.setView(dialogView);

        // Get the custom alert dialog view widgets reference
        Button btn_positive = (Button) dialogView.findViewById(R.id.dialog_positive_btn);
        Button btn_negative = (Button) dialogView.findViewById(R.id.dialog_negative_btn);
        final EditText et_name = (EditText) dialogView.findViewById(R.id.et_name);

        // Create the alert dialog
        final AlertDialog dialog = builder.create();

        // Set positive/yes button click listener
        btn_positive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Dismiss the alert dialog
                dialog.cancel();
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        });

        // Set negative/no button click listener
        btn_negative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Dismiss/cancel the alert dialog
                //dialog.cancel();
                dialog.dismiss();
                File file = new File(Environment.getExternalStorageDirectory() + "/Download/"+fileName+".csv");
                file.delete();
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        });

        // Display the custom alert dialog on interface
        Button recordButt = (Button) findViewById(R.id.recordButton);
        if (recordButt.getVisibility() == View.VISIBLE)
        {
            dialog.show();
        }
        else
        {
            android.os.Process.killProcess(android.os.Process.myPid());
        }

    }

    private boolean isBusy = false;//this flag to indicate whether your async task completed or not
    private boolean stop = false;//this flag to indicate whether your button stop clicked
    private Handler handler = new Handler();


    //Handler to show wifi list dynamicly
        public void startHandler() {
            handler.postDelayed(new Runnable() {

                @Override
                public void run() {

                    if (!isBusy) callAysncTask();

                    if (!stop) startHandler();

                }
            }, 100);
        }

        private void callAysncTask() {
            //TODO
            new AsyncTaskRunner().execute();
        }




    //Asynctask to show wifi dynamicly
    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private String resSSid = "";
        private String resRss = "";
        private String resp = "";


        @Override
        protected String doInBackground(String... params) {

            final WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
            int state = wifi.getWifiState();
            if (state == WifiManager.WIFI_STATE_ENABLED) {
                wifi.startScan();
                List<ScanResult> results = wifi.getScanResults();
                resp ="";
                resSSid = "";
                resRss = "";
                for (ScanResult scanResult : results) {
                    int level = WifiManager.calculateSignalLevel(scanResult.level, 10);

                    resp = resp + String.valueOf(scanResult.SSID) + " :   " + String.valueOf(level) + "\n";
                    resSSid = resSSid + String.valueOf(scanResult.SSID) + "\n";
                    resRss = resRss + String.valueOf(level) + "\n";

                }
            }
            return resp;
        }


        @Override
        protected void onPostExecute(String result) {
            finalSSid.setText(resSSid);
            finalRss.setText(resRss);
            isBusy = false;
        }


        @Override
        protected void onPreExecute() {
            isBusy = true;
        }


        @Override
        protected void onProgressUpdate(String... text) {
            finalSSid.setText(resSSid);
            finalRss.setText(resRss);
        }
    }
}
